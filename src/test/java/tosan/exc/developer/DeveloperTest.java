package tosan.exc.developer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tosan.exc.excercise.ExcerciseApplication;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ExcerciseApplication.class)
class DeveloperTest {

    private MockMvc mvc;

    @Autowired
    public DeveloperTest(MockMvc mvc) {
        this.mvc = mvc;
    }

    @Test
    void getDevelopers () throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/developers");
        mvc.perform(request).andExpect(status().isOk())
                            .andDo(print());
    }

    @Test
    void addDeveloper() throws Exception {
        Developer developer = new Developer("Zahra", "Ghandi", Type.CLIENT);
        ObjectMapper mapper = new ObjectMapper();
        String jsonContent = mapper.writeValueAsString(developer);

        RequestBuilder request = MockMvcRequestBuilders.post("/developers")
                .content(jsonContent)
                .contentType(MediaType.APPLICATION_JSON);
        mvc.perform(request).andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void editDeveloper() throws Exception {
        long id = 1;
        Developer developer = new Developer(id,"Zahra", "Ghandi", Type.CLIENT);
        ObjectMapper mapper = new ObjectMapper();
        String jsonContent = mapper.writeValueAsString(developer);

        RequestBuilder request = MockMvcRequestBuilders.put("/developers/".concat(String.valueOf(id)))
                .content(jsonContent)
                .contentType(MediaType.APPLICATION_JSON);
        mvc.perform(request).andExpect(status().isOk())
                .andDo(print());
    }
}