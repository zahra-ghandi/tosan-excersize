package tosan.exc.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tosan.exc.excercise.ExcerciseApplication;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ExcerciseApplication.class)
class TaskTest {

    private MockMvc mvc;

    @Autowired
    public TaskTest(MockMvc mvc) {
        this.mvc = mvc;
    }

    @Test
    void getTasks() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/tasks");
        mvc.perform(request).andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void addTask() throws Exception {
        String jsonContent = "{\n" +
                "\t\"title\": \"test\",\n" +
                "\t\"taskDescription\": \"testing\",\n" +
                "\t\"tastType\": \"CLIENT\",\n" +
                "\t\"neededTime\": 3600,\n" +
                "  \"spentTime\":1800,\n" +
                "\t\"taskState\": \"WAITING\",\n" +
                "\t\"taskStateDescription\": \"ddd\",\n" +
                "  \"finishTime\": \"2017-11-01T00:00:00\"\n" +
                "}";

        RequestBuilder request = MockMvcRequestBuilders.post("/tasks")
                .content(jsonContent)
                .contentType(MediaType.APPLICATION_JSON);
        mvc.perform(request).andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void editTask() throws Exception {
        String jsonContent = "\"id\": 1,\n" +
                "    \"title\": \"testinggg\",\n" +
                "    \"taskDescription\": \"testing\",\n" +
                "    \"taskType\": \"CLIENT\",\n" +
                "    \"neededTime\": 1800,\n" +
                "    \"spentTime\": 3600,\n" +
                "    \"taskState\": \"WAITING\",\n" +
                "    \"taskStateDescription\": \"ddd\",\n" +
                "    \"finishTime\": null,\n" +
                "    \"developer\": null";

        RequestBuilder request = MockMvcRequestBuilders.put("/tasks/1")
                .content(jsonContent)
                .contentType(MediaType.APPLICATION_JSON);
        mvc.perform(request).andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void assignTask() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.patch("/tasks/2/assign/1");
        mvc.perform(request).andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void changeTaskState() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.patch("/tasks/1/state/WAITING");
        mvc.perform(request).andExpect(status().isOk())
                .andDo(print());
    }
}