package tosan.exc.excercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {
		"tosan.exc.developer",
		"tosan.exc.task"})
@EntityScan(basePackages = {
		"tosan.exc.developer",
		"tosan.exc.task"})
@EnableJpaRepositories(basePackages = {
		"tosan.exc.developer",
		"tosan.exc.task"})
@Configuration
public class ExcerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcerciseApplication.class, args);
	}

}
