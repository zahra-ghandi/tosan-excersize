package tosan.exc.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tosan.exc.developer.Developer;
import tosan.exc.developer.Type;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService {

    private TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> getTasks() {
        List<Task> tasks = new ArrayList<>();
        taskRepository.findAll().forEach(t -> tasks.add(t));
        return tasks;
    }

    public void addTask(Task task) {
        taskRepository.save(task);
    }

    public void editTask(Task task) {
        taskRepository.save(task);
    }

    public void assignTask(long taskId, long devId) {
        Task task = taskRepository.findById(taskId).get();
        task.setDeveloper(new Developer(devId, "", "", Type.CLIENT));
        taskRepository.save(task);
    }

    public void changeTaskState(long id, String state) {
        Task task = taskRepository.findById(id).get();
        task.setTaskState(TaskState.valueOf(state));
        taskRepository.save(task);
    }

}
