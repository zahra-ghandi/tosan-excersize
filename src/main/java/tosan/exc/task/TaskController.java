package tosan.exc.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path="tasks")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public List<Task> getTasks() {
        return taskService.getTasks();
    }

    @PostMapping
    public void addTask(@RequestBody Task task) {
        taskService.addTask(task);
    }

    @PutMapping("{id}")
    public void editTask(@RequestBody Task task, @PathVariable long id) {
        taskService.editTask(task);
    }

    @PatchMapping("{taskId}/assign/{devId}")
    public void assignTask(@PathVariable long taskId, @PathVariable long devId) {
        taskService.assignTask(taskId, devId);
    }

    @PatchMapping("{id}/state/{state}")
    public void changeTaskState(@PathVariable(value="id") long id, @PathVariable(value="state") String state) {
        taskService.changeTaskState(id, state);
    }

}
