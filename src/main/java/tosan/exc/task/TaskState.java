package tosan.exc.task;

public enum TaskState {
    IN_PROGRESS, BLOCKED, WAITING, CANCELLED, UNSTARTED, UNASSIGNED
}
