package tosan.exc.task;

import tosan.exc.developer.Developer;
import tosan.exc.developer.Type;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
public class Task {
    @Id
    @GeneratedValue
    private long id;
    private String title;
    private String taskDescription;
    private Type taskType;
    private long neededTime;
    private long spentTime;
    private TaskState taskState;
    private String taskStateDescription;
    private LocalDateTime finishTime;

    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "developer_id")
    @JoinColumn()
    private Developer developer;

    public Task() {}

    public Task(String title, String taskDescription, Type taskType, TaskState taskState, String taskStateDescription) {
        this.title = title;
        this.taskDescription = taskDescription;
        this.taskType = taskType;
        this.taskState = taskState;
        this.taskStateDescription = taskStateDescription;
    }

    public Task(String title, String taskDescription, Type taskType, long neededTime, long spentTime, TaskState taskState, String taskStateDescription, LocalDateTime finishTime) {
        this.title = title;
        this.taskDescription = taskDescription;
        this.taskType = taskType;
        this.neededTime = neededTime;
        this.spentTime = spentTime;
        this.taskState = taskState;
        this.taskStateDescription = taskStateDescription;
        this.finishTime = finishTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Type getTaskType() {
        return taskType;
    }

    public void setTaskType(Type taskType) {
        this.taskType = taskType;
    }

    public long getNeededTime() {
        return neededTime;
    }

    public void setNeededTime(long neededTime) {
        this.neededTime = neededTime;
    }

    public long getSpentTime() {
        return spentTime;
    }

    public void setSpentTime(long spentTime) {
        this.spentTime = spentTime;
    }

    public TaskState getTaskState() {
        return taskState;
    }

    public void setTaskState(TaskState taskState) {
        this.taskState = taskState;
    }

    public String getTaskStateDescription() {
        return taskStateDescription;
    }

    public void setTaskStateDescription(String taskStateDescription) {
        this.taskStateDescription = taskStateDescription;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }
}
