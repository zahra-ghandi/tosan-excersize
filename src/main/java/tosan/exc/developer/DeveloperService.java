package tosan.exc.developer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tosan.exc.task.Task;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeveloperService {

    private DeveloperRepository developerRepository;

    @Autowired
    public DeveloperService(DeveloperRepository developerRepository) {
        this.developerRepository = developerRepository;
    }

    public List<Developer> getDevelopers() {
        List<Developer> developers = new ArrayList<>();
        developerRepository.findAll().forEach(d -> developers.add(d));
        return developers;
    }

    public void addDeveloper(Developer developer) {
        developerRepository.save(developer);
    }

    public void editDeveloper(Developer developer) {
        developerRepository.save(developer);
    }
}
