package tosan.exc.developer;

import tosan.exc.task.Task;

import javax.persistence.*;
import java.util.List;

@Entity
public class Developer {
    @Id
    @GeneratedValue
    private long id;
    private String firstName;
    private String lastName;
    private Type devType;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "developer")
    private List<Task> tasks;


    public Developer() {}

    public Developer(String firstName, String lastName, Type devType) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.devType = devType;
    }

    public Developer(long id, String firstName, String lastName, Type devType) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.devType = devType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Type getDevType() {
        return devType;
    }

    public void setDevType(Type devType) {
        this.devType = devType;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
