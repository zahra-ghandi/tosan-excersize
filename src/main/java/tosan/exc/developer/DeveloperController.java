package tosan.exc.developer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path="developers")
public class DeveloperController {

    private DeveloperService developerService;

    @Autowired
    public DeveloperController(DeveloperService developerService) {
        this.developerService = developerService;
    }

    @GetMapping
    public List<Developer> getDevelopers() {
        return developerService.getDevelopers();
    }

    @PostMapping
    public void addDeveloper(@RequestBody Developer developer) {
        developerService.addDeveloper(developer);
    }

    @PutMapping("{id}")
    public void editDeveloper(@RequestBody Developer developer, @PathVariable long id) {
        developerService.editDeveloper(developer);
    }
}
